# first-draft 0.1.3

FOSS live drawing and annotation tool

[![pipeline status](https://gitlab.com/bytesnz/first-draft/badges/master/pipeline.svg)](https://gitlab.com/bytesnz/first-draft/commits/master)
[![first-draft on NPM](https://bytes.nz/b/first-draft/npm)](https://npmjs.com/package/first-draft)
[![developtment time](https://bytes.nz/b/first-draft/custom?color=yellow&name=development+time&value=~48+hours)](https://gitlab.com/MeldCE/first-draft/blob/master/.tickings)

# INTIAL DEVELOPMENT - NOT STABLE!

**This package is under heavy initial development. Once it is ready to be
used in production, it will be published as a non-zero major version.**

## Demo

A demo is currently available at https://demo.first-draft.xyz

## Using

To use

```
yarn add first-draft
# Create a folder for the drafts to be stored in
mkdir drafts
npx first-draft
```

or globally

```

yarn global add first-draft

# Create a folder for the drafts to be stored in

mkdir drafts
first-draft
```

## Development

Gitlab is being used for managing the development of this projact.

- The [Feature Board](https://gitlab.com/MeldCE/first-draft/boards/982414) shows the list of features being developed
- The [Version Board](https://gitlab.com/MeldCE/first-draft/boards/982432) shows the user stories planned for each [release](https://gitlab.com/MeldCE/first-draft/milestones)
- The [Development Board](https://gitlab.com/MeldCE/first-draft/boards/971250) shows the process of user stories
- The [Bug List](https://gitlab.com/MeldCE/first-draft/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=type%3A%20bug) shows any bugs that are currently outstanding

# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## Unreleased

### Added

- Pan/Zoom tool with global using middle click or shift click
- Cancelling of a path using the pen with a right click or escape
- Click select/move/delete tool
- Click pen tool

### Fixed

- Sizing in mobile browsers
- Cancelled line not being removed from other connected clients

## [0.1.3] - 2019-03-13

### Added

- Added demo link to README

### Fixed

- Add bang to first-draft command so it runs

## [0.1.2] - 2019-03-10

Initial release.

Features:

- pen tool
- saving to JSON/folder

## 0.0.1 - 2019-03-02

Initial placeholder release

[0.1.3]: https://gitlab.com/MeldCE/first-draft/compare/v0.1.2...v0.1.3
[0.1.2]: https://gitlab.com/MeldCE/first-draft/compare/v0.0.1...v0.1.2
[0.0.1]: https://gitlab.com/MeldCE/first-draft/tree/v0.0.1
