# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## Unreleased

### Added

- Pan/Zoom tool with global using middle click or shift click
- Cancelling of a path using the pen with a right click or escape
- Click select/move/delete tool
- Click pen tool

### Fixed

- Sizing in mobile browsers
- Cancelled line not being removed from other connected clients

## [0.1.3] - 2019-03-13

### Added

- Added demo link to README

### Fixed

- Add bang to first-draft command so it runs

## [0.1.2] - 2019-03-10

Initial release.

Features:

- pen tool
- saving to JSON/folder

## 0.0.1 - 2019-03-02

Initial placeholder release

[0.1.3]: https://gitlab.com/MeldCE/first-draft/compare/v0.1.2...v0.1.3
[0.1.2]: https://gitlab.com/MeldCE/first-draft/compare/v0.0.1...v0.1.2
[0.0.1]: https://gitlab.com/MeldCE/first-draft/tree/v0.0.1
