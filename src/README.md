# first-draft <!=package version>

FOSS live drawing and annotation tool

[![pipeline status](https://gitlab.com/bytesnz/first-draft/badges/master/pipeline.svg)](https://gitlab.com/bytesnz/first-draft/commits/master)
[![first-draft on NPM](https://bytes.nz/b/first-draft/npm)](https://npmjs.com/package/first-draft)
[![developtment time](https://bytes.nz/b/first-draft/custom?color=yellow&name=development+time&value=~48+hours)](https://gitlab.com/MeldCE/first-draft/blob/master/.tickings)

# INTIAL DEVELOPMENT - NOT STABLE!

**This package is under heavy initial development. Once it is ready to be
used in production, it will be published as a non-zero major version.**

## Demo

A demo is currently available at https://demo.first-draft.xyz

## Using

To use

```
yarn add first-draft
# Create a folder for the drafts to be stored in
mkdir drafts
npx first-draft
```

or globally

```

yarn global add first-draft

# Create a folder for the drafts to be stored in

mkdir drafts
first-draft
```

## Development

Gitlab is being used for managing the development of this projact.

- The [Feature Board](https://gitlab.com/MeldCE/first-draft/boards/982414) shows the list of features being developed
- The [Version Board](https://gitlab.com/MeldCE/first-draft/boards/982432) shows the user stories planned for each [release](https://gitlab.com/MeldCE/first-draft/milestones)
- The [Development Board](https://gitlab.com/MeldCE/first-draft/boards/971250) shows the process of user stories
- The [Bug List](https://gitlab.com/MeldCE/first-draft/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=type%3A%20bug) shows any bugs that are currently outstanding

<!=include CHANGELOG.md>
