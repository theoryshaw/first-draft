const s4 = () => {
  return Math.floor((1 + Math.random()) * 0x10000)
    .toString(32)
    .substring(1);
};

export const id = (length: number = 9): string => {
  const iterations = Math.ceil(length / 3);

  let idString = '';

  let i = 0;
  while (i++ < iterations) {
    idString += s4();
  }

  if (length % 3) {
    return idString.slice(0, length + 1);
  } else {
    return idString;
  }
};
export default id;
