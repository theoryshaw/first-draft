/**
 * Get a value from deep inside an object
 *
 * @param object Object to get value from
 * @param path Path to traverse to get value
 *
 * @returns The deep value or undefined if value or something in the path
 *   doesn't exist
 */
export const getDeep = (
  object: { [key: string]: any },
  path: Array<string | number>
): any => {
  if (!path || !path.length) {
    return object;
  }

  while (typeof object === 'object' && path.length) {
    object = object[path.shift()];
  }

  if (path.length) {
    return;
  }

  return object;
};
