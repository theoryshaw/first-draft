import * as Draft from './Draft';

interface BaseMessage {
  id: string;
}

interface BaseDrawMessage extends BaseMessage {
  action: string;
  user?: any;
}

export interface CurrentDrawing extends BaseMessage {
  draft?: Draft;
}

export interface CreateElement extends BaseDrawMessage {
  action: 'create';
  element: Draft.DraftElement;
  layerName: string;
  index?: number;
}

export interface ModifyElement extends BaseDrawMessage {
  action: 'modify';
  element: Draft.DraftElement;
  layerName: string;
}

export interface PositionElements extends BaseDrawMessage {
  action: 'position';
  elements: Array<{
    elementName: string;
    layerName: string;
    position: [number, number];
  }>;
}

export interface DeleteElement extends BaseDrawMessage {
  action: 'delete';
  elementName: string;
  layerName: string;
}

export interface DeleteMultipleElements extends BaseDrawMessage {
  action: 'deleteMultiple';
  elements: Array<{
    elementName: string;
    layerName: string;
  }>;
}

export interface CreateLayer extends BaseDrawMessage {
  action: 'createLayer';
  layer: Draft.BaseElement;
  index?: number;
}

export interface ModifyLayer extends BaseDrawMessage {
  action: 'modifyLayer';
  layer: Draft.BaseElement;
}

export interface DeleteLayer extends BaseDrawMessage {
  action: 'deleteLayer';
  layerName: string;
}

export type DraftMessage =
  | CreateElement
  | ModifyElement
  | PositionElement
  | DeleteElement
  | DeleteMultipleElements
  | CreateLayer
  | ModifyLayer
  | DeleteLayer;

export interface AuthChallenge extends BaseMessage {
  challenge?: string;
  username?: string;
}
