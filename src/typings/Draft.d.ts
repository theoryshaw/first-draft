type Matrix = [number, number, number, number, number, number];

type Segment = [[number, number], [number, number], [number, number], number];

export interface BaseElement {
  name?: string;
  data?: {
    label?: string;
    permission?: Array<Permission>;
  };
}

interface BasePositionalElement extends BaseElement {
  type: string;
  matrix?: Matrix;
}

interface RasterElement extends BasePositionalElement {
  type: 'Raster';
  source: string;
}

interface PathElement extends BasePositionalElement {
  type: 'Path';
  segments: Array<Segment>;
}

export interface Layer extends BaseElement {
  children: Array<DraftElement>;
}

export type DraftElement = RasterElement | PathElement;

interface Permission {
  type: 'user' | 'group';
  permissions: Array<string>;
}

export interface Drawing {
  layers: Array<Layer>;
}

export interface Draft {
  name?: string;
  description?: string;
  created?: number;
  createdBy?: string;
  tools?: {
    toolbar?: Array<string>;
    mapping?: any;
  };
  permissions?: Array<Permission>;
  drawings: Array<Drawing>;
  version: number;
}
