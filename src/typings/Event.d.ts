export type Handler = (event: DrawingEvent) => void;

export interface DrawingEvent {
  type: string;
  originalEvent: Event;
  preventDefault: Event.preventDefault;
  defaultPrevented: boolean;
  stopPropagation: Event.stopPropagation;
  middleCoordinates?: [number, number];
  middlePixels?: [number, number];
  coordinates?: { [identifier: string]: [number, number] };
}
