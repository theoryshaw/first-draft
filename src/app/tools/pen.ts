interface PenContext {
  initialCoordinates?: Coordinates;
  attachmentType?: AttachmentType;
  path?: Path;
  guide?: Path;
}

const addPointToLine = (instance, event, context: PenContext) => {
  if (!context.path) {
    context.path = instance.createPath();
    context.guide = instance.createPath(false);
  } else {
    context.line.coordinates(event.middleCoordinates);
  }
};

const updateGuide = (instance, event, context: PenContext) => {
  context.guide.removePoint();
  context.guid.addPoint(event.middleCoordinates);
};

export default <Tool>{
  name: 'Pen',
  description: 'Pen tool',
  icon: '',
  cursor: 'crosshair',
  draggable: true,
  mappings: [
    {
      active: true,
      type: 'mousedown',
      filter: {
        button: 0,
        movement: true
      },
      action: 'start',
      chainActions: [
        {
          action: 'update',
          type: 'mousemove'
        },
        {
          action: 'stop',
          type: 'mouseup',
          filter: {
            button: 0
          }
        },
        {
          action: 'cancel',
          type: 'keyup',
          filter: {
            key: 'Delete'
          }
        },
        {
          action: 'cancel',
          type: 'keyup',
          filter: {
            key: 'Escape'
          }
        },
        {
          type: 'contextmenu'
        },
        {
          action: 'cancel',
          type: 'mouseup',
          filter: {
            button: 2
          }
        }
      ]
    },
    {
      active: true,
      type: 'click',
      filter: {
        button: 0
      },
      action: 'click',
      chainActions: [
        {
          action: 'cancel',
          type: 'keyup',
          filter: {
            key: 'Delete'
          }
        },
        {
          action: 'cancel',
          type: 'keyup',
          filter: {
            key: 'Escape'
          }
        },
        {
          type: 'contextmenu'
        },
        {
          action: 'stop',
          type: 'mouseup',
          filter: {
            button: 2
          }
        },
        {
          type: 'mousedown',
          filter: {
            button: 0,
            movement: true
          },
          action: 'click',
          chainActions: [
            {
              action: 'update',
              type: 'mousemove'
            },
            {
              type: 'mouseup',
              endChain: true
            }
          ]
        }
      ]
    },
    {
      active: true,
      type: 'touchstart',
      filter: {
        touches: 1
      },
      action: 'start',
      chainActions: [
        {
          action: 'update',
          type: 'touchmove',
          filter: {
            touches: 1
          }
        },
        {
          action: 'stop',
          type: 'touchend',
          filter: {
            touches: 0
          }
        }
      ]
    }
  ],
  actions: {
    start: {
      handler: (instance, event, context: PenContext) => {
        instance.deselectAll();
        (context.initialCoordinates = event.middleCoordinates),
          (context.attachmentType = 'drag'),
          (context.path = instance.createPath());
        context.path.addPoint(event.middleCoordinates, true);
        return true;
      }
    },
    update: {
      handler: (instance, event, context: PenContext) => {
        context.path.addPoint(event.middleCoordinates, true);
        return true;
      }
    },
    click: {
      handler: (instance, event, context: PenContext) => {
        console.log('pen click handler called');
        if (!context.path) {
          instance.deselectAll();
          (context.initialCoordinates = event.middleCoordinates),
            (context.attachmentType = 'click'),
            (context.path = instance.createPath());
        }
        context.path.addPoint(event.middleCoordinates, true);
        return true;
      }
    },
    stop: {
      handler: (instance, event, context: PenContext) => {
        if (event.middleCoordinates) {
          context.path.addPoint(event.middleCoordinates, true);
        }
        context.path.finish();
      }
    },
    cancel: {
      handler: (instance, event, context: PenContext) => {
        context.path.remove();
      }
    }
  }
};
