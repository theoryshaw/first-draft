const moveHandler = (instance, event, context, complete?: boolean) => {
  if (context.startPosition) {
    const offset = event.middleCoordinates.subtract(context.startPosition);

    if (offset.length || context.moved) {
      context.moved = true;
      const newPositions = context.initialPositions.map((item) => ({
        item: item.item,
        position: item.position.add(offset)
      }));

      instance.moveItems(newPositions, !complete);
    }

    return true;
  }
};

export default <Tool>{
  name: 'Select',
  description: 'Select/move tool',
  icon: 'select',
  cursor: 'pointer',
  mappings: [
    {
      type: 'keyup',
      filter: {
        key: 'Delete'
      },
      action: 'delete'
    },
    {
      active: true,
      type: 'mousedown',
      filter: {
        button: 0
      },
      parameters: {
        add: 'ctrlKey'
      },
      action: 'startMove',
      chainActions: [
        {
          action: 'updateMove',
          type: 'mousemove'
        },
        {
          action: 'finishMove',
          type: 'mouseup'
        }
      ]
    },
    {
      type: 'keyup',
      filter: {
        key: 'Escape'
      },
      action: 'clearSelection'
    }
  ],
  actions: {
    clearSelection: {
      handler: (instance, event) => {
        instance.deselectAll();
      }
    },
    delete: {
      handler: (instance, event) => {
        instance.deleteSelected();
      }
    },
    startMove: {
      handler: (instance, event, context) => {
        if (event.item) {
          if (!event.item.selected) {
            if (!event.parameters.add) {
              instance.deselectAll();
            }
            event.item.selected = true;
          }

          const selected = instance.selected;

          context.startPosition = event.middleCoordinates;
          context.initialPositions = selected.map((item) => ({
            item,
            position: item.position
          }));

          return true;
        } else {
          if (!event.parameters.add) {
            instance.deselectAll();
          }
        }
      }
    },
    updateMove: {
      handler: moveHandler
    },
    finishMove: {
      handler: (instance, event, context) => {
        moveHandler(instance, event, context, true);
        return false;
      }
    }
  }
};
