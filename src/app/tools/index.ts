import pen from './pen';
import panZoom from './pan-zoom';
import select from './select';

export default {
  pen,
  panZoom,
  select
};
