export default <Tool>{
  name: 'Pan/Zoom',
  description: 'Pan and Zoom Tool',
  icon: '',
  cursor: 'grab',
  draggable: true,
  buttons: [
    {
      label: 'Zoom to extent',
      action: 'zoomToExtent'
    },
    {
      label: 'Reset pan/zoom',
      action: 'resetPanAndZoom'
    }
  ],
  mappings: [
    {
      active: true,
      type: 'mousedown',
      filter: {
        button: 0
      },
      action: 'start',
      chainActions: [
        {
          action: 'update',
          type: 'mousemove'
        },
        {
          action: 'stop',
          type: 'mouseup'
        }
      ]
    },
    {
      type: 'wheel',
      action: 'zoom'
    },
    {
      type: 'mousedown',
      filter: {
        $or: {
          button: 1,
          $and: {
            button: 0,
            shiftKey: true
          }
        }
      },
      action: 'start',
      chainActions: [
        {
          action: 'update',
          type: 'mousemove'
        },
        {
          action: 'stop',
          type: 'mouseup'
        }
      ]
    },
    {
      active: true,
      type: 'touchstart',
      filter: {
        touches: 1
      },
      action: 'start',
      chainActions: [
        {
          action: 'update',
          type: 'touchmove',
          filter: {
            touches: 1
          }
        },
        {
          action: 'stop',
          type: 'touchend',
          filter: {
            touches: 0
          }
        }
      ]
    },
    {
      type: 'touchstart',
      filter: {
        touches: 2
      },
      action: 'start',
      chainActions: [
        {
          action: 'update',
          type: 'touchmove',
          filter: {
            touches: 2
          }
        },
        {
          action: 'stop',
          type: 'touchend'
        }
      ]
    }
  ],
  actions: {
    start: {
      handler: (instance, event, context?: PanContext) => {
        (context.originalOffset = instance.offset),
          (context.initialViewCoordinates = event.middleCoordinates.add(
            instance.offset
          )),
          (context.cursor = instance.getStyle('cursor'));
        instance.setStyle('cursor', 'grabbing');
        return true;
      }
    },
    update: {
      handler: (instance, event, context: PanContext) => {
        if (event.middleCoordinates) {
          const delta = event.middleCoordinates
            .add(instance.offset)
            .subtract(context.initialViewCoordinates);
          instance.scrollBy(delta, context.originalOffset);
        }
        return true;
      }
    },
    stop: {
      handler: (instance, event, context: PanContext) => {
        instance.setStyle('cursor', context.cursor);
      }
    },
    zoom: {
      handler: (instance, event) => {
        if (event.originalEvent.deltaY) {
          if (event.originalEvent.deltaY < 0) {
            instance.zoomIn(event.middleCoordinates);
          } else {
            instance.zoomOut(event.middleCoordinates);
          }
          event.preventDefault();
        }
      }
    },
    zoomin: {
      handler: (instance, event) => {
        // For pinch use distance between touches relative to initial distance
        // betwee ntouches
        instance.zoomIn();
      }
    },
    zoomout: {
      handler: (instance, event) => {
        instance.zoomOut();
      }
    },
    resetZoom: {
      handler: (instance, event) => {
        instance.zoom(1);
      }
    },
    resetPan: {
      handler: (instance, event) => {
        instance.setOffset(0, 0);
      }
    },
    resetPanAndZoom: {
      handler: (instance, event) => {
        instance.zoom(1);
        instance.setOffset(0, 0);
      }
    },
    zoomToExtent: {
      handler: (instance, event) => {
        instance.zoomToExtent();
      }
    }
  }
};
