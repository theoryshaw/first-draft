import { App } from './typings/app';
import { Draft } from '../../typings/Draft';

import * as tools from '../tools';

export const createTools = (el: Element, app: App) => {
  let activeTool = null;
  let toolElements = {};

  const generateToolButton = (id, tool) => {
    if (!toolElements[id]) {
      toolElements[id] = document.createElement('button');
      toolElements[id].title = tool.description || tool.name;
      const icon = document.createElement('i');
      icon.innerHTML = tool.name;
      icon.className = `fa fa-${tool.icon}`;
      toolElements[id].appendChild(icon);
      toolElements[id].addEventListener('click', () => {
        if (activeTool !== id) { // TODO Check if tool is disabled
          const oldActiveTool = activeTool;
          activeTool = id;
          tool.activate().then((shouldActivate) => {
            if (!shouldActivate) {
              activeTool = oldActiveTool;
            }
          });
        }
      });
    }
    toolElements[id].setAttribute('aria-selected', id === activeTool);

    return toolElements[id];
  };

  const redrawToolbar = () => {
    el.innerHTML = '';

    /*todo.forEach([id, tool] => {
      const element = generateToolButton(id, tool);
      el.appendChild(element);
    });*/
  };

  return {
    /**
     * Pass an event to the toolbar for it to handle
     *
     * @param event Event to handle
     */
    event: (event: DraftEvent) => {
    },
    /**
     * Chnage draft
     */
    updateTools: (draft: Draft) => {
      if (draft.tools) {
        if (draft.tools.toolbar) {

    }
  };
};
