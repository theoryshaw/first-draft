import tools from '../tools';
import { getDeep } from '../../lib/utils';

interface EventAction {
  tool: string;
  mapping: Mapping;
  parentEventAction?: EventAction;
  eventsCache: Event[];
  startCoordinates: Point | null;
  neededMovement?: boolean | null;
  movement?: boolean | null;
  context: { [key: string]: any };
  chainEventActions: Array<{
    type: string;
    eventAction: EventAction;
  }>;
}

interface EventActions {
  [event: string]: Array<{
    element: Element;
    eventAction: EventAction;
  }>;
}

const mouseMoveThreshold = 0;
const touchMoveThreshold = 10;

const documentEvents = ['keyup', 'keydown', 'paste'];
const movementFilterTypes = ['mousedown', 'mouseup', 'touchstart', 'touchstop'];
const movementCheckEvents = [
  'mousedown',
  'mousemove',
  'mouseup',
  'touchstart',
  'touchmove',
  'touchend'
];

/**
 * Check if an event matches the filters
 *
 * @param event Event to check
 * @param filter Filter to check event against
 * @param or Only require one filter to match
 *
 * @returns Whether the event matches the filters
 */
const eventMatchesFilters = (event, filter, or: boolean = false) => {
  // If or, return if a filter matches, if not, return if a filter doesn't
  // match
  if (event.originalEvent) {
    event = event.originalEvent;
  }

  const result = Object.entries(filter).find(([property, value]) => {
    switch (property) {
      case '$or':
      case '$and':
        if (or) {
          return eventMatchesFilters(event, value, property === '$or');
        } else {
          return !eventMatchesFilters(event, value, property === '$or');
        }
        break;
      case 'button':
      case 'shiftKey':
      case 'ctrlKey':
      case 'altKey':
        if (event[property] === value) {
          return or;
        }
        break;
      case 'key':
        if (event.code === value) {
          return or;
        }
        break;
      case 'touches':
        if (event.touches && event.touches.length === value) {
          return or;
        }
        break;
      case 'movement':
      default:
        return or;
    }

    return !or;
  });

  return Boolean(result) === or;
};

const mappingNeedsMovement = (mapping) =>
  mapping.type === 'click' ||
  (movementFilterTypes.indexOf(mapping.type) !== -1 &&
    mapping.filter &&
    typeof mapping.filter.movement === 'boolean');

export const createTools = (
  app: App,
  options,
  drawings: Array<DrawingManager>
) => {
  const eventActions: EventActions = {};
  let activeTool = null;
  let toolElements = {};
  let toolbar: HTMLElement = options.toolbar;

  /**
   * Add the additional events to the element for detecting movement
   *
   * TODO Use setPointerCapture instead of this
   *
   * @param event Event that triggered the adding of additional events
   * @param eventAction Event action to add/remove
   * @param remove Whether the eventAction should be removed instead of
   *   added
   */
  const additionalEventActionListeners = (
    triggerEvent: Event,
    eventAction: EventAction,
    remove?: boolean
  ) => {
    const element = triggerEvent.currentTarget;
    let events;

    if (triggerEvent.type.startsWith('mouse')) {
      events = ['mousemove', 'mouseup'];
    } else if (triggerEvent.type.startsWith('touch')) {
      events = ['touchmove', 'touchend'];
    }

    if (remove) {
      events.forEach((type) => {
        doRemoveListener(type, eventAction.mapping, element);
      });
    } else {
      events.forEach((type) => {
        doAddListener(type, eventAction, element);
      });
    }
  };

  /**
   * Event handler that is used to handle all events
   *
   * @param event Event to handle
   */
  // TODO With document events. Need to only action event if correct drawing
  // Maybe add target drawing to EventAction?
  const eventHandler = (
    event: Event,
    ignoreEventActions?: EventAction[]
  ): boolean => {
    let handled = false;
    if (eventActions[event.type]) {
      eventActions[event.type].find((elementAction) => {
        if (
          !elementAction ||
          elementAction.element !== event.currentTarget ||
          (ignoreEventActions &&
            ignoreEventActions.indexOf(elementAction.eventAction) !== -1)
        ) {
          return;
        }

        const eventAction = elementAction.eventAction;
        const element = elementAction.element;

        // TODO Need a better way to do this
        let drawing;
        if (element.canvas) {
          drawing = element;
        } else {
          drawing = drawings[0];
        }

        // Check if filter matches
        if (
          eventAction.mapping.filter &&
          !eventMatchesFilters(event, eventAction.mapping.filter)
        ) {
          return;
        }

        // Add parameters
        if (eventAction.mapping.parameters) {
          event.parameters = {};
          Object.entries(eventAction.mapping.parameters).forEach(
            ([parameter, value]) => {
              if (typeof value === 'function') {
                event.parameters[parameter] = value(event);
              } else if (typeof event.originalEvent[value] !== 'undefined') {
                event.parameters[parameter] = event.originalEvent[value];
              }
            }
          );
        }

        if (
          eventAction.movement === null &&
          ['mousedown', 'touchstart'].indexOf(event.type) !== -1
        ) {
          if (eventAction.eventsCache.length) {
            // TODO Do something with cached events?
          }

          // Reset eventAction
          eventAction.eventsCache = [event];
          eventAction.movement = null;
          eventAction.startPixels = event.middlePixels;
          eventAction.startCoordinates = event.middleCoordinates;

          // Add listeners for move and up/end
          if (event.type.startsWith('mouse')) {
            ['mousemove', 'mouseup'].forEach((type) => {
              doAddListener(type, eventAction, element);
            });
          } else if (event.type.startsWith('touch')) {
            ['touchmove', 'touchend'].forEach((type) => {
              doAddListener(type, eventAction, element);
            });
          }
          handled = true;
        } else if (
          eventAction.neededMovement !== null &&
          eventAction.movement === null
        ) {
          // Check movement
          // Check for movement
          let threshold;
          if (event.type.startsWith('touch')) {
            threshold = touchMoveThreshold;
          } else if (event.type.startsWith('mouse')) {
            threshold = mouseMoveThreshold;
          }

          // Calculate movement based on event coordinates
          const movement = Math.sqrt(
            Math.pow(event.middlePixels.x - eventAction.startPixels.x, 2) +
              Math.pow(event.middlePixels.y - eventAction.startPixels.y, 2)
          );

          if (movement > threshold) {
            eventAction.movement = true;
          }

          if (['mousemove', 'touchmove'].indexOf(event.type) !== -1) {
            if (eventAction.movement) {
              // Remove listeners (real ones will be added if keeping events)
              additionalEventActionListeners(event, eventAction, true);

              // If detected movement when we don't want it, remove and clear
              if (!eventAction.neededMovement) {
                // Replay the events so another handler can handle them
                eventAction.eventsCache.find(
                  (cachedEvent) =>
                    !eventHandler(cachedEvent, [
                      ...(ignoreEventActions || []),
                      eventAction
                    ])
                );
                eventAction.eventsCache = [];
              } else {
                // Add current event to cache so will be replayed
                eventAction.eventsCache.push(event);

                // Replay the events
                eventAction.eventsCache.forEach((cachedEvent) => {
                  eventHandler(cachedEvent);
                });

                // Remove the events in the cache
                eventAction.eventsCache = [];

                // TODO XXX Do some other way
                const finishEvent =
                  event.type === 'mousemove' ? 'mouseup' : 'touchend';
                const endHandler = () => {
                  eventAction.movement = null;
                  drawing.removeEventListener(finishEvent, endHandler);
                };
                drawing.addEventListener(finishEvent, endHandler);
              }
            }
          } else if (['mouseup', 'touchend'].indexOf(event.type) !== -1) {
            if (!eventAction.movement) {
              eventAction.movement = false;

              if (eventAction.mapping.type === 'click') {
                // Replay first event
                runAction(drawing, eventAction.eventsCache[0], eventAction);

                // Clear the previous events
                eventAction.eventsCache = [];

                eventAction.movement = null;
              } else if (eventAction.neededMovement === false) {
                // Add current event to cache so will be replayed
                eventAction.eventsCache.push(event);

                // Replay the events
                eventAction.eventsCache.forEach((cachedEvent) => {
                  eventHandler(cachedEvent);
                });

                // Remove the events in the cache
                eventAction.eventsCache = [];
              } else {
                // Add current event to cache so will be replayed
                eventAction.eventsCache.push(event);

                // Replay the events so another handler can handle them
                // as long as they are handled
                eventAction.eventsCache.find(
                  (cachedEvent) =>
                    !eventHandler(cachedEvent, [
                      ...(ignoreEventActions || []),
                      eventAction
                    ])
                );

                // Ignore and clear the events
                eventAction.eventsCache = [];
              }

              // Remove listeners (real ones will be added if keeping events)
              additionalEventActionListeners(event, eventAction, true);

              eventAction.movement = null;
            } else {
              console.warn('TODO got movement unexpectedly');
            }
          }
          handled = true;
        } else {
          // Run action
          runAction(drawing, event, eventAction);
          handled = true;
        }
        return handled;
      });

      if (handled) {
        event.preventDefault();
      }
    }

    return handled;
  };

  const runAction = (drawing, event, eventAction: EventAction) => {
    const action =
      eventAction.mapping.action &&
      getDeep(tools, [
        eventAction.tool,
        'actions',
        eventAction.mapping.action,
        'handler'
      ]);

    const parentEventAction = eventAction.parentEventAction || eventAction;
    let mainEventAction = parentEventAction;
    while (mainEventAction.parentEventAction) {
      mainEventAction = mainEventAction.parentEventAction;
    }

    let inChain = null;
    if (action) {
      inChain = action(drawing, event, mainEventAction.context) || false;
    } else if (eventAction.mapping.endChain) {
      inChain = false;
    }

    // Create the chain even handlers if we have a context
    if (
      inChain !== false &&
      !eventAction.chainEventActions &&
      eventAction.mapping.chainActions
    ) {
      eventAction.mapping.chainActions.forEach((chainAction) => {
        addMappingListener(
          mainEventAction.tool,
          chainAction,
          drawing,
          eventAction
        );
      });
      eventAction.chainEventActions = true;
    } else if (!inChain && parentEventAction.chainEventActions) {
      parentEventAction.mapping.chainActions.forEach((chainAction) => {
        removeMappingListener(mainEventAction.tool, chainAction, drawing);
      });
      parentEventAction.chainEventActions = false;
      if (parentEventAction === mainEventAction) {
        mainEventAction.context = {};
      }
    }
  };

  /**
   * Add handlers for a given mapping to the given elements (drawings)
   *
   * @param tool Tool the mapping is from
   * @param mapping Mapping to add the handlers for
   * @param elements Elements to add the handlers to,  or null to add the
   *   handlers to all drawings
   */
  const addMappingListener = (
    tool: string,
    mapping,
    elements?,
    parentEventAction?
  ) => {
    if (documentEvents.indexOf(mapping.type) !== -1) {
      elements = [document];
    } else if (!elements) {
      elements = drawings;
    } else if (!Array.isArray(elements)) {
      elements = [elements];
    }

    elements.forEach((element) => {
      // If have a movement dependent event, add Listener to mousedown and
      // touchstart to detect (the lack of) movement before adding for the real
      // handler
      if (mappingNeedsMovement(mapping)) {
        const neededMovement =
          mapping.type === 'click' ? false : mapping.filter.movement;
        const action: EventAction = {
          tool,
          mapping,
          parentEventAction,
          eventsCache: [],
          startCoordinates: null,
          neededMovement,
          movement: null,
          context: {}
        };
        ['mousedown', 'touchstart'].forEach((event) => {
          doAddListener(event, action, element, Boolean(parentEventAction));
        });
      } else {
        const action: EventAction = {
          tool,
          mapping,
          parentEventAction,
          startCoordinates: null,
          context: {}
        };
        doAddListener(
          mapping.type,
          action,
          element,
          Boolean(parentEventAction)
        );
      }
    });
  };

  /**
   * Called by addMappingListener and eventHandler to do the actual adding of
   * the eventAction and handler to the eventActions and the document
   *
   * @param event Event type
   * @param action EventAction to add
   */
  const doAddListener = (
    event: string,
    action: EventAction,
    element,
    priority
  ) => {
    if (!eventActions[event]) {
      // TODO Is there a benefit to attach to document? Will mean checking
      // that it is the targetted canvas harder
      element.addEventListener(event, eventHandler);
      eventActions[event] = [
        {
          element,
          eventAction: action
        }
      ];
    } else {
      // Check to see if there is already an eventAction for this element
      const index = eventActions[event].findIndex(
        (elementAction) => elementAction.element === element
      );

      let elementEventAction;

      if (index === -1) {
        element.addEventListener(event, eventHandler);
      } else {
        // Find if already have eventActions for the given element
        elementEventAction = eventActions[event].find(
          (elementAction) =>
            elementAction.element === element &&
            elementAction.eventAction === action
        );
      }

      if (!elementEventAction) {
        if (priority) {
          eventActions[event].unshift({
            element,
            eventAction: action
          });
        } else {
          eventActions[event].push({
            element,
            eventAction: action
          });
        }
      }
    }
  };

  /**
   * Remove a mapping from the given elements (drawings)
   *
   * @param tool Tool the mapping is from
   * @param mapping Mapping to remove the handlers for
   * @param elements Elements to remove the handlers to,  or null to add the
   *   handlers to all drawings
   */
  const removeMappingListener = (tool: string, mapping, elements?) => {
    if (documentEvents.indexOf(mapping.type) !== -1) {
      elements = [document];
    } else if (!elements) {
      elements = drawings;
    } else if (!Array.isArray(elements)) {
      elements = [elements];
    }

    elements.forEach((element) => {
      if (mappingNeedsMovement(mapping)) {
        // Ensure removed from all potential event listeners
        movementCheckEvents.forEach((event) => {
          doRemoveListener(event, mapping, element);
        });
      } else {
        doRemoveListener(mapping.type, mapping, element);
      }
    });
  };

  /**
   * Called by removeMappingListener to do the actual removing of a
   * mapping and the handler from eventActions and the document
   *
   * @param event Event type to remove the mapping from
   * @param mapping Mapping to remove
   * @param element Element to remove the mapping for
   */
  const doRemoveListener = (event: string, mapping: Mapping, element) => {
    // Find mapping for element
    if (eventActions[event]) {
      let index = eventActions[event].findIndex(
        (elementAction) =>
          elementAction.element === element &&
          elementAction.eventAction.mapping === mapping
      );

      if (index !== -1) {
        if (eventActions[event].length === 1) {
          // Remove listener and event actions as will be empty
          element.removeEventListener(event, eventHandler);
          delete eventActions[event];
        } else {
          eventActions[event].splice(index, 1);

          // Check if there are any other eventActions for this element
          index = eventActions[event].findIndex(
            (elementAction) => elementAction.element === element
          );

          if (index === -1) {
            element.removeEventListener(event, eventHandler);
          }
        }
      }
    }
  };

  /**
   * Setup the new active tool once it has been confirmed it should be
   * activated
   *
   * @param activeTool New active tool
   * @param oldActiveTool Previous active tool
   */
  const setupTool = (newActiveTool, oldActiveTool) => {
    const cursor = tools[newActiveTool].cursor || '';
    drawings.forEach((drawing) => {
      drawing.canvas.style.cursor = cursor;
    });

    attachListeners(newActiveTool, oldActiveTool);

    if (newActiveTool && toolElements[newActiveTool]) {
      toolElements[newActiveTool].setAttribute('aria-selected', true);
    }
    if (oldActiveTool && toolElements[oldActiveTool]) {
      toolElements[oldActiveTool].setAttribute('aria-selected', false);
    }
  };

  /**
   * Add event handlers to the drawing and the document required for the
   * tool mappings
   *
   * @param activeTool New active tool
   * @param oldActiveTool Previous active tool
   */
  const attachListeners = (activeTool, oldActiveTool) => {
    if (oldActiveTool && tools[oldActiveTool].mappings) {
      tools[oldActiveTool].mappings.forEach((mapping) => {
        if (mapping.active) {
          removeMappingListener(oldActiveTool, mapping);
        }
      });
    }

    if (activeTool && tools[activeTool].mappings) {
      tools[activeTool].mappings.forEach((mapping) => {
        if (mapping.active) {
          addMappingListener(activeTool, mapping);
        }
      });
    }
  };

  /**
   * Generate the HTML Button for the given tool
   *
   * @param id The ID of the tool
   * @param tool The tool
   *
   * @returns The generated HTML element
   */
  const generateToolButton = (id, tool) => {
    if (!toolElements[id]) {
      toolElements[id] = document.createElement('button');
      toolElements[id].title = tool.description || tool.name;
      const icon = document.createElement('i');
      icon.innerHTML = tool.name;
      icon.className = `fa fa-${tool.icon}`;
      toolElements[id].appendChild(icon);
      toolElements[id].addEventListener('click', () => {
        if (activeTool !== id) {
          // TODO Check if tool is disabled
          const oldActiveTool = activeTool;
          activeTool = id;
          if (tool.activate) {
            tool.activate().then((shouldActivate) => {
              if (!shouldActivate) {
                activeTool = oldActiveTool;
              } else {
                setupTool(activeTool, oldActiveTool);
              }
            });
          } else {
            setupTool(activeTool, oldActiveTool);
          }
        }
      });

      toolElements[id].setAttribute('aria-selected', id === activeTool);
    }

    return toolElements[id];
  };

  const generateToolActionButton = (id, tool, button) => {
    const handler = getDeep(tool, ['actions', button.action, 'handler']);
    if (!handler) {
      return;
    }
    const buttonElement = document.createElement('button');
    buttonElement.title = button.description || button.label;
    const icon = document.createElement('i');
    icon.innerHTML = button.label;
    icon.className = `fa fa-${tool.icon}`;
    buttonElement.appendChild(icon);
    buttonElement.addEventListener('click', (event) => {
      if (!button.active || activeTool === id) {
        // TODO active drawing
        handler(drawings[0], event);
      }
    });

    return buttonElement;
  };

  const redrawToolbar = () => {
    if (!toolbar) {
      return;
    }

    toolbar.innerHTML = '';

    Object.entries(tools).forEach(([id, tool]) => {
      const element = generateToolButton(id, tool);
      toolbar.appendChild(element);

      if (tool.buttons) {
        tool.buttons.forEach((button) => {
          const buttonEl = generateToolActionButton(id, tool, button);
          if (buttonEl) {
            toolbar.appendChild(buttonEl);
          }
        });
      }
    });
  };

  if (toolbar) {
    redrawToolbar();
  }

  // Add non-active handlers
  Object.entries(tools).forEach(([name, tool]) => {
    if (tool.mappings) {
      tool.mappings.forEach((mapping) => {
        if (!mapping.active) {
          addMappingListener(name, mapping);
        }
      });
    }
  });

  const instance = {
    /**
     * Change draft
     */
    updateTools: (draft: Draft) => {
      if (draft.tools) {
        if (draft.tools.toolbar) {
          redrawToolbar();
        }
      }
    },
    remove: () => {
      // Remove all listeners
      Object.entries(listeners).forEach(([type, listener]) => {
        if (listener.handlers) {
          listener.handlers.forEach((handler) => {
            handler.element.removeEventListener(type, handler.handler);
          });
        }
      });
    }
  };

  return instance;
};
