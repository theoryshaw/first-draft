import { App } from './typings/app';
import { createDrawing } from './drawing/paper';
import { createTools } from './tools';
import * as Messages from '../../typings/Message';

import makeId from '../../lib/id';

interface Options {
  id: string;
  toolbar?: Toolbar;
  status?: Status;
}

const createNewDraft = (): Draft => ({
  drawings: [
    {
      layers: [
        {
          name: 'images',
          children: []
        },
        {
          name: 'drawing',
          children: []
        }
      ]
    }
  ],
  version: 1
});

/**
 * TODO Change canvas to be containing div. Then each drawing will create its
 * own canvas (or whatever it needs
 */
export const startDraft = (
  canvas: Element,
  app: App,
  config: Config,
  options: Options
) => {
  let initial: boolean = true;
  let drawing;
  let tools;
  let draft;
  let requestId;

  app.socket.on('connect', () => {
    requestId = makeId();
    const request = {
      id: requestId,
      name: options.id,
      getDraft: initial
    };
    app.socket.emit('draft:connect', request);
  });

  app.socket.on('draft:error', (data) => {
    console.error('error received', data);
  });

  app.socket.on('draft:current', (data: Messages.CurrentDrawing) => {
    if (data.draft) {
      if (data.draft.name) {
        document.title = `${data.draft.name} - first-draft`;
      }
    }

    if (initial) {
      initial = false;
      if (data.draft) {
        draft = data.draft;
        drawing = createDrawing(canvas, app, draft, draft.drawings[0]);
      } else {
        draft = createNewDraft();
        drawing = createDrawing(canvas, app, draft, draft.drawings[0]);
        app.socket.emit('draft:create', {
          name: options.id,
          draft
        });
      }

      drawing.addEventListener('draw', (event) => {
        app.socket.emit('draft:draw', event);
      });

      tools = createTools(app, options, [drawing]);
    }
  });

  app.socket.on('draft:view', (data) => {});

  // Get the draft
  /*{
    requestId = makeId()
    const request = {
      id: requestId,
      name: options.id
    };
    app.socket.emit('draft:connect', request);
  }*/

  if (options.toolbar) {
  }
};
