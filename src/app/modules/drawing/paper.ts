import { App } from '../../../typings/app';
import Paper from 'paper/dist/paper-core.js';
import * as DraftEvents from '../../../typings/Event';
import * as Draft from '../../../typings/Draft';
import ResizeObserver from 'resize-observer-polyfill';

import makeId from '../../../lib/id';

export interface DrawingManager {
  addEventListener: (type: string, handler: Event.Handler) => void;
  removeEventListener: (type: string, handler: Event.Handler) => void;
}

const internalEvents = ['draw'];
const hitTestEvents = ['mousedown', 'click', 'touchstart'];

export const createDrawing = (
  canvas: Element,
  app: App,
  draft: Draft.Draft,
  drawing?: Draft.Drawing
) => {
  const paper = new Paper.PaperScope();
  paper.setup(canvas);
  let imagesLayer;
  let drawingLayer;
  let textLayer;
  // Offset of the 0,0 point of drawing from the view 0,0 point
  // -ve means the top-left of the drawing off the top-left of the view
  let offset = new paper.Point([0, 0]);
  const eventHandlers = {
    draw: []
  };

  paper.settings.hitTolerance = 5;

  /**
   * Update the center of the drawing based on the offset
   */
  const updateCenter = () => {
    const centerOffset = new paper.Point(
      paper.view.viewSize.divide(2).divide(paper.view.zoom)
    );
    paper.view.center = centerOffset.subtract(offset);
    updateCanvasPostion();
  };

  /**
   * Update the offset based on the center of the drawing
   */
  const resetOffset = () => {
    offset = new paper.Point(
      paper.view.viewSize
        .divide(2)
        .divide(paper.view.zoom)
        .subtract(paper.view.center)
    );
    updateCanvasPostion();
  };

  const updateZoom = (amount: number, point?: Point) => {
    const currentZoom = paper.view.zoom;
    const newZoom = Math.min(10, Math.max(0.05, amount));

    if (currentZoom !== newZoom) {
      if (point) {
        // Calculate the real pixel distance between the point and the center
        const delta = point.subtract(paper.view.center);
        // at the current zoom
        const currentRealDelta = delta.multiply(currentZoom);
        // at the new zoom
        const newRealDelta = delta.multiply(newZoom);
        // Subtract and convert back into canvas pixels
        const difference = newRealDelta
          .subtract(currentRealDelta)
          .divide(newZoom);
        // Change zoom
        paper.view.zoom = newZoom;
        // Adjust canvas center based on difference
        paper.view.center = paper.view.center.add(difference);
      } else {
        paper.view.zoom = newZoom;
      }

      // Update offset for new zoom
      resetOffset();
    }
  };

  const updateCanvasPostion = () => {
    const postionEl = document.getElementById('canvasPosition');

    if (postionEl) {
      postionEl.innerText =
        Math.round(-offset.x || 0) +
        ', ' +
        Math.round(-offset.y || 0) +
        ' (' +
        paper.view.zoom.toFixed(1) +
        'x)';
    }
  };

  const getFullBounds = () => {
    let minX = 0,
      maxX = 0,
      minY = 0,
      maxY = 0;

    paper.project.layers.forEach((layer) => {
      const bounds = layer.strokeBounds;

      if (bounds.width !== 0 || bounds.height !== 0) {
        minX = Math.min(bounds.x, minX);
        maxX = Math.max(bounds.x + bounds.width, maxX);
        minY = Math.min(bounds.y, minY);
        maxY = Math.max(bounds.y + bounds.height, maxY);
      }
    });

    return new paper.Rectangle({
      x: minX,
      y: minY,
      width: maxX - minX,
      height: maxY - minY
    });
  };

  const zoomToExtent = () => {
    const bounds = getFullBounds();
    const viewSize = paper.view.viewSize;

    if (bounds.width || bounds.height) {
      const scale = Math.min(
        1,
        viewSize.width / bounds.width,
        viewSize.height / bounds.height
      );
      paper.view.zoom = scale;
      offset = new paper.Point(-bounds.x, -bounds.y);
      updateCenter();
    }
  };

  /**
   * Convert clientX and clientY values into drawing coordinates
   *
   * @param clientX clientX coordinate
   * @param clientY clientY coordinate
   *
   * @returns The coordinates in the drawing [x, y]
   */
  const toDrawingCoordinates = (clientX: number, clientY: number) => {
    const rect = canvas.getBoundingClientRect();

    return [
      (clientX - rect.left) / paper.view.zoom - offset.x,
      (clientY - rect.top) / paper.view.zoom - offset.y
    ];
  };

  /**
   * Convert x and y drawing coordinates into clientX and clientY coordinates
   *
   * @returns The client coordinates of the coordinates in the drawing or
   *   null if the coordinates are not visible
   */
  const fromDrawingCoordinates = (x: number, y: number) => {
    const rect = canvas.getBoundingClientRect();

    let clientX = (x - offset.x) * paper.view.zoom;
    let clientY = (y - offset.y) * paper.view.zoom;

    if (
      clientX < 0 ||
      clientX > rect.width ||
      clientY < 0 ||
      clientY > rect.height
    ) {
      return null;
    }

    return [
      (clientX - rect.left) / paper.view.zoom + offset.x,
      (clientY - rect.top) / paper.view.zoom + offset.y
    ];
  };

  /**
   * Convert a standard event into a DrawingEvent
   *
   * @param event Standard event
   *
   * @returns DrawingEvent
   */
  const drawingEvent = (event: Event) => {
    if (!eventHandlers[event.type]) {
      console.log('no event handlers?');
      return;
    }

    const drawingEvent: DraftEvents.DrawingEvent = {
      currentTarget: thisDrawing,
      type: event.type,
      originalEvent: event,
      get defaultPrevented() {
        return event.defaultPrevented;
      },
      preventDefault: () => event.preventDefault(),
      stopPropagation: () => event.stopPropagation()
    };

    if (typeof event.clientX === 'number') {
      drawingEvent.middleCoordinates = new paper.Point(
        toDrawingCoordinates(event.clientX, event.clientY)
      );
      drawingEvent.middlePixels = new paper.Point(
        toDrawingCoordinates(event.clientX, event.clientY)
      );
      if (hitTestEvents.indexOf(event.type) !== -1) {
        const hitResult = paper.project.hitTest(drawingEvent.middleCoordinates);
        if (hitResult && hitResult.item) {
          drawingEvent.item = hitResult.item;
        }
      }
    } else if (event.touches) {
      if (event.touches.length) {
        let x = 0;
        let y = 0;

        for (let i = 0; i < event.touches.length; i++) {
          x += event.touches[i].clientX;
          y += event.touches[i].clientY;
        }

        x = x / event.touches.length;
        y = y / event.touches.length;

        drawingEvent.middlePixels = new paper.Point(x, y);
        drawingEvent.middleCoordinates = new paper.Point(
          toDrawingCoordinates(x, y)
        );

        if (
          event.touches.length === 1 &&
          hitTestEvents.indexOf(event.type) !== -1
        ) {
          const hitResult = paper.project.hitTest(
            drawingEvent.middleCoordinates
          );
          if (hitResult && hitResult.item) {
            drawingEvent.item = hitResult.item;
          }
        }
      } else {
        drawingEvent.middleCoordinates = null;
      }
    }

    // Duplicate the handlers array so can iterate through the array without
    // it being changed while running through the handlers
    const handlers = [...eventHandlers[event.type]];
    handlers.forEach((handler, index) => {
      // Check the handler is still in the event handlers
      if (eventHandlers[event.type].indexOf(handler) === -1) {
        return;
      }

      handler(drawingEvent);
    });
  };

  const emit = (type, event) => {
    event.type = type;

    if (eventHandlers[type]) {
      eventHandlers[type].forEach((handler) => handler(event));
    }
  };

  const debounceTimeouts = {};

  const debounceEmit = (type, event) => {
    if (!debounceTimeouts[type]) {
      debounceTimeouts[type] = {
        timeout: setTimeout(() => {
          emit(type, debounceTimeouts[type].event);
          delete debounceTimeouts[type];
        }, 100),
        event
      };
    } else {
      debounceTimeouts[type].event = event;
    }
  };

  // Set up ResizeObserver
  const resizeObserver = new ResizeObserver(() => {
    const bounds = canvas.getBoundingClientRect();
    if (canvas.width !== bounds.width || canvas.height !== bounds.height) {
      canvas.width = bounds.width;
      canvas.height = bounds.height;
      paper.view.setViewSize(bounds);
      updateCenter();
    }
  });
  resizeObserver.observe(canvas);

  if (drawing) {
    // Load the drawing in
    paper.project.importJSON(
      drawing.layers.map((layer) => [
        'Layer',
        {
          ...layer,
          children: layer.children.map((element) => [element.type, element])
        }
      ])
    );
    imagesLayer = paper.project.layers.find((layer) => layer.name === 'images');
    drawingLayer = paper.project.layers.find(
      (layer) => layer.name === 'drawing'
    );

    // TODO Add config check
    zoomToExtent();
  } else {
    // Create 3 default layers - text, drawing and images
    imagesLayer = new paper.Layer({ name: 'images' });
    drawingLayer = new paper.Layer({ name: 'drawing' });
  }

  // Attach for draw events
  app.socket.on('draft:draw', (data) => {
    if (typeof data !== 'object' || data === null) {
      return;
    }
    // TODO Check if event is for this drawing

    if (data.action === 'position' && data.elements) {
      data.elements.forEach((element) => {
        const layer = paper.project.layers.find(
          (layer) => layer.name === element.layerName
        );

        if (layer) {
          const item = layer.children.find(
            (item) => item.name === element.elementName
          );

          if (item) {
            item.position = new paper.Point(element.position);
          }
        }
      });

      return;
    } else if (data.action === 'deleteMultiple' && data.elements) {
      data.elements.forEach((element) => {
        const layer = paper.project.layers.find(
          (layer) => layer.name === element.layerName
        );
        if (!layer) {
          // TODO out of sync?
          return;
        }

        const item = layer.children.find(
          (item) => item.name === element.elementName
        );

        if (!item) {
          // TODO out of sync?
          return;
        }

        item.remove();
      });
      return;
    }

    {
      // Find the layer
      const layerIndex = paper.project.layers.findIndex(
        (layer) => layer.name === data.layerName
      );
      const layer = layerIndex !== -1 ? paper.project.layers[layerIndex] : null;

      if (data.action === 'createLayer') {
        // Add layer to drawing
        paper.project.addLayer(data.element);
        return;
      }

      if (layer) {
        if (data.action === 'delete') {
          const item = layer.children.find(
            (item) => item.name === data.elementName
          );

          if (!item) {
            // TODO out of sync?
            return;
          }

          item.remove();
          return;
        }

        // Try and find element if already in the layer
        const index = layer.children.findIndex(
          (element) => element.name === data.element.name
        );

        switch (data.action) {
          case 'create':
          case 'modify':
            let item;
            // Create item from element
            switch (data.element.type) {
              case 'Path':
                item = new paper.Path(data.element);
                break;
            }

            if (index === -1) {
              if (typeof data.index === 'number') {
                layer.insertChild(index, item);
              } else {
                layer.addChild(item);
              }
            } else {
              layer.children[index].replaceWith(item);
            }
            break;
        }
      }
    }
  });

  updateCanvasPostion();

  const thisDrawing: DrawingManager = {
    addEventListener: (type, handler) => {
      if (eventHandlers[type]) {
        eventHandlers[type].push(handler);
      } else {
        eventHandlers[type] = [handler];
        if (internalEvents.indexOf(type) === -1) {
          canvas.addEventListener(type, drawingEvent);
        }
      }
    },
    removeEventListener: (type, handler) => {
      if (eventHandlers[type]) {
        const index = eventHandlers[type].indexOf(handler);
        if (index !== -1) {
          eventHandlers[type].splice(index, 1);
          if (!eventHandlers[type].length) {
            delete eventHandlers[type];
            if (internalEvents.indexOf(type) === -1) {
              canvas.removeEventListener(type, drawingEvent);
            }
          }
        }
      }
    },
    get canvas() {
      return canvas;
    },
    get drawing() {
      const copy = paper.project
        .exportJSON({ asString: false })
        .filter(([type, item]) => type === 'Layer')
        .map(([type, item]) => item);
      return copy;
    },
    selectAll: () => {
      paper.project.selectAll();
    },
    deselectAll: () => {
      paper.project.deselectAll();
    },
    get selected() {
      return paper.project.selectedItems;
    },
    deleteSelected: () => {
      const ids = [];
      paper.project.selectedItems.map((item) => {
        ids.push({
          elementName: item.name,
          layerName: item.layer.name
        });
        item.remove();
      });

      emit('draw', {
        action: 'deleteMultiple',
        elements: ids
      });
    },
    get offset() {
      return offset;
    },
    setOffset: (x: number, y: number) => {
      offset = new paper.Point(x, y);
      updateCenter();
    },
    getStyle: (style: string) => {
      return canvas.style[style];
    },
    setStyle: (style: string, value: any) => {
      canvas.style[style] = value;
    },
    scrollBy: (delta: Point, point?: Point) => {
      if (point === true) {
        offset = new paper.Point(0, 0).add(delta);
        updateCenter();
      } else if (point) {
        offset = point.add(delta);
        updateCenter();
      } else {
        offset = offset.add(delta);
        updateCenter();
      }
    },
    zoomIn: (point?: Point, amount?: number) => {
      if (!amount || amount < 0) {
        amount = 0.1;
      }

      updateZoom(paper.view.zoom + amount * paper.view.zoom, point);
    },
    zoomOut: (point?: Point, amount?: number) => {
      if (!amount || amount < 0) {
        amount = -0.1;
      }

      updateZoom(paper.view.zoom + amount * paper.view.zoom, point);
    },
    zoom: (amount: number, point?: Point) => {
      updateZoom(amount, point);
    },
    zoomToExtent: () => {
      zoomToExtent();
    },
    moveItems: (items, active) => {
      const positions = items.map((item) => {
        item.item.position = item.position;
        return {
          elementName: item.item.name,
          layerName: item.item.layer.name,
          position: [item.position.x, item.position.y]
        };
      });

      debounceEmit('draw', {
        action: 'position',
        elements: positions,
        active
      });
    },
    createPath: (options = {}, share: boolean = true) => {
      options = {
        strokeColor: 'black',
        strokeWidth: 2,
        strokeCap: 'round',
        ...options
      };
      let timeout = null;
      let lastActive = false;
      const path = new paper.Path({ ...options, selected: true });
      const id = makeId();
      path.name = id;
      const layerName = paper.project.activeLayer.name;

      const pathEmit = (active: boolean = false) => {
        lastActive = active;
        // TODO send emit instance on path completion
        // Debounce emitting of events for performance
        if (timeout === null) {
          timeout = setTimeout(() => {
            // TODO simplify as progressing?
            // Paper exports as [ 'Path', object ], so just grab object
            const pathObject = path.exportJSON({ asString: false })[1];
            emit('draw', {
              action: 'create',
              layerName,
              element: {
                ...options,
                type: 'Path',
                name: id,
                matrix: pathObject.matrix,
                segments: pathObject.segments
              },
              active: lastActive
            });
            timeout = null;
          }, 100);
        }
      };

      return {
        addPoint: (point, drawing?: boolean) => {
          path.add(point);
          if (share) {
            pathEmit(drawing);
          }
        },
        removePoint: (index, drawing?: boolean) => {
          path.removeSegment(index);
          if (share) {
            pathEmit(drawing);
          }
        },
        remove: () => {
          path.remove();
          if (timeout !== null) {
            clearTimeout(timeout);
            timeout = null;
          }
          // Emit a delete
          emit('draw', {
            action: 'delete',
            layerName,
            elementName: id,
            active: true
          });
        },
        finish: () => {
          path.simplify(10);
          if (share) {
            pathEmit();
          }
        }
      };
    },
    remove: () => {
      Object.entries(eventHandlers).forEach(([type, handlers]) => {
        if (internalEvents.indexOf(type) === -1) {
          handlers.forEach((handler) => canvas.removeEventListener(handler));
        }
      });

      if (resizeObserver) {
        resizeObserver.unobserve(canvas);
        resizeObserver.disconnect();
      }
    }
  };

  return thisDrawing;
};
