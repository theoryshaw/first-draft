import * as paper from 'paper/dist/paper-core';
import urlJoin from 'url-join';
import io from 'socket.io-client';

import { startDraft } from './modules/draft';
import { createStatus } from './modules/status';
import { createUser } from './modules/user';

import makeId from '../lib/id';

let givenConfig = typeof window.CONFIG === 'object' ? window.CONFIG : {};

const config = {
  baseUri: '/',
  drawingUri: 'd',
  create: true,
  getUser: true,
  ...givenConfig
};

const socket = io();

const pathRegex = new RegExp(
  `^${urlJoin(config.baseUri, config.drawingUri, '(.+)')}$`
);

let id;
const match = pathRegex.exec(location.pathname);
if (!match) {
  if (config.create) {
    id = makeId();
    history.replaceState(
      { id },
      '',
      urlJoin(config.baseUri, config.drawingUri, id)
    );
  }
} else {
  id = match[1];
}

const app = {
  config,
  socket: {
    emit: (event, data) => {
      socket.emit(
        event,
        data
          ? {
              ...data,
              senderId: socket.id
            }
          : null
      );
    },
    on: (event, handler) => {
      socket.on(event, (data) => {
        if (!data || data.senderId !== socket.id) {
          handler(data);
        }
      });
    }
  }
};

const toolbar = document.getElementById('toolbar');
const status = createStatus(document.getElementById('footer'), app);
startDraft(document.getElementById('canvas'), app, config, {
  id,
  toolbar,
  status
});
