import * as express from 'express';
import * as socketIO from 'socket.io';
import * as http from 'http';
import * as Bundler from 'parcel-bundler';
import { resolve } from 'path';
import { addDraftEvents } from './modules/draft';

const app = express();
const server = new http.Server(app);
const io = socketIO(server);

const config = {};

const parcelOptions = {
  https: false,
  hmr: true
};

const entry = resolve(__dirname, '../app/index.html');

console.log('entry is', entry);
const bundler = new Bundler(entry, parcelOptions);

console.log('Adding parcel as middleware');
app.use(bundler.middleware());

console.log('setting up sockets');
io.on('connection', (socket) => {
  console.log('got a connection, adding events');
  const draft = addDraftEvents(socket, config);

  socket.on('disconnect', () => {
    draft.disconnect();
  });
});

const port = Number(process.env.PORT) || 7067;
const host = process.env.HOST || 'localhost';

server.listen(port, host, () => {
  console.log(`First Draft started on ${host}:${port}`);
});
