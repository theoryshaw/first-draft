#!/bin/env node
import * as express from 'express';
import * as socketIO from 'socket.io';
import * as http from 'http';
import { resolve, join } from 'path';
import { addDraftEvents } from './modules/draft';
import { constants as fsConstants, accessSync } from 'fs';

const app = express();
const server = new http.Server(app);
const io = socketIO(server);

const config = {};

// TODO Make better
const dist = resolve(__dirname, '../dist');

// Check if the drafts folder exists
try {
  accessSync(resolve('drafts'), fsConstants.R_OK | fsConstants.W_OK);
} catch (error) {
  console.error('Could not access drafts folder');
  process.exit(1);
}

app.get('/d/*', (req, res) => {
  res.sendFile(join(dist, 'index.html'));
});

app.use(express.static(dist));

io.on('connection', (socket) => {
  const draft = addDraftEvents(socket, config);

  socket.on('disconnect', () => {
    draft.disconnect();
  });
});

const port = Number(process.env.PORT) || 7067;
const host = process.env.HOST || 'localhost';

server.listen(port, host, () => {
  console.log(`First Draft started on ${host}:${port}`);
});
