import Queue from 'rowdy-fs';
import * as fs from 'fs';
import * as path from 'path';
import { promisify } from 'util';
import { Draft } from '../../typings/Draft';
import { DraftMessage } from '../../typings/Message';

const access = promisify(fs.access);
const mkdir = promisify(fs.mkdir);

interface Instance {
  close(): void;
  update(data: DraftMessage): Promise<Draft>;
}

interface Cache {
  queue: Queue;
  instances: Array<Instance>;
}

interface Storage {
  haveDraft(id: string): Promise<boolean>;
  loadDraft(id: string): Promise<Instance>;
}

export const createStorage = (config): Storage => {
  const draftsDir = path.resolve(config.draftsDir || 'drafts');
  const cachedDrafts: {
    [id: string]: { queue: Queue; subscribers: number };
  } = {};

  const haveDraft = (id: string) => {
    return access(path.join(draftsDir, id), fs.constants.R_OK).then(
      () => {
        return Promise.resolve(true);
      },
      (error) => {
        if (error.code === 'ENOENT') {
          return Promise.resolve(false);
        }

        return Promise.reject(error);
      }
    );
  };

  const draftFile = (id: string) => path.join(draftsDir, id, 'draft.json');
  const draftFolder = (id: string) => path.join(draftsDir, id);

  return {
    haveDraft,
    loadDraft: (id: string) => {
      return haveDraft(id)
        .then((have) => {
          if (!have) {
            return mkdir(draftFolder(id));
          }
        })
        .then(() => {
          let cachedDraft;
          if (!cachedDrafts[id]) {
            cachedDraft = new Queue(draftFile(id));
            cachedDrafts[id] = {
              queue: cachedDraft,
              subscribers: 1
            };
          } else {
            cachedDraft = cachedDrafts[id];
            cachedDrafts[id].subscribers++;
          }

          const instance = {
            close() {
              if (cachedDraft) {
                cachedDrafts[id].subscribers--;
                if (!cachedDrafts[id].subscribers) {
                  delete cachedDrafts[id];
                }
              }
            },
            read() {
              return cachedDraft.read().then((content) => {
                try {
                  const draft = JSON.parse(content);
                  // TODO Validate draft with schema
                  return draft;
                } catch (error) {
                  return Promise.reject(
                    `Error reading draft: ${error.message}`
                  );
                }
              });
            },
            save(data) {
              return cachedDraft.write(JSON.stringify(data.draft));
            },
            update(data: DraftMessage) {
              return cachedDraft.modify((contents) => {
                let draft;
                if (!contents) {
                  return Promise.reject('No draft to update');
                }

                // Convert to Object
                try {
                  draft = JSON.parse(contents);
                } catch (error) {
                  return Promise.reject('Error parsing existing drawing');
                }

                if (!draft || !draft.drawings || !draft.drawings[0]) {
                  return Promise.reject('No drawing');
                }

                const drawing = draft.drawings[0];

                let index;
                let layer;

                switch (data.action) {
                  case 'create':
                    layer = drawing.layers.find(
                      (layer) => layer.name === data.layerName
                    );
                    index = layer.children.findIndex(
                      (element) => element.name === data.element.name
                    );

                    if (index !== -1) {
                      return Promise.reject('Duplicate element ID');
                    }

                    layer.children.push(data.element);
                    break;
                  case 'modify':
                    layer = drawing.layers.find(
                      (layer) => layer.name === data.layerName
                    );
                    index = layer.children.findIndex(
                      (element) => element.name === data.element.name
                    );

                    if (index === -1) {
                      return Promise.reject('Could not find element in layer');
                    }

                    layer.children[index] = data.element;
                    break;
                  case 'position':
                    data.elements.forEach((element) => {
                      const layer = drawing.layers.find(
                        (layer) => layer.name === element.layerName
                      );
                      if (!layer) {
                        // TODO out of sync?
                        return;
                      }

                      const item = layer.children.find(
                        (item) => item.name === element.elementName
                      );

                      if (!item) {
                        // TODO out of sync?
                        return;
                      }

                      item.position = element.position;
                    });
                    break;
                  case 'deleteMultiple':
                    data.elements.forEach((element) => {
                      const layer = drawing.layers.find(
                        (layer) => layer.name === element.layerName
                      );
                      if (!layer) {
                        // TODO out of sync?
                        return;
                      }

                      const index = layer.children.findIndex(
                        (item) => item.name === element.elementName
                      );

                      if (index === -1) {
                        // TODO out of sync?
                        return;
                      }

                      layer.children.splice(index, 1);
                    });
                    break;
                  case 'delete':
                    layer = drawing.layers.find(
                      (layer) => layer.name === data.layerName
                    );
                    index = layer.children.findIndex(
                      (element) => element.name === data.elementName
                    );

                    if (index !== -1) {
                      layer.children.splice(index, 1);
                    }
                    break;
                  case 'createLayer':
                    index = drawing.layers.findIndex(
                      (layer) => layer.name === data.layer.name
                    );

                    if (index !== -1) {
                      return Promise.reject('Duplicate layer ID');
                    }

                    drawing.layers.push(data.layer);
                    break;
                  case 'modifyLayer':
                    index = drawing.layers.findIndex(
                      (layer) => layer.name === data.layer.name
                    );

                    if (index === -1) {
                      return Promise.reject('Could not find layer');
                    }

                    drawing.layers[index] = {
                      ...drawing.layers[index],
                      ...data.layer
                    };
                    break;
                  case 'deleteLayer':
                    index = drawing.layers.findIndex(
                      (layer) => layer.name === data.layerName
                    );

                    if (index === -1) {
                      return Promise.reject('Could not find layer');
                    }

                    drawing.layers.splice(index, 1);
                    break;
                }

                if (data.user) {
                  // TODO Add last modified?
                }

                return JSON.stringify(draft);
              });
            }
          };

          return instance;
        });
    }
  };
};
