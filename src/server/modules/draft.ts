import { createStorage } from '../storage/file';

export const addDraftEvents = (socket, config) => {
  //const user = createUserProvider(socket, config);
  let user = null;
  const storage = createStorage(config);
  let draftId = null;
  let draftStore = null;

  const closeOpen = () => {
    if (draftId && draftStore) {
      socket.leave(`draft:${draftId}`);
      draftStore.close();
      draftId = null;
      draftStore = null;
    }
  };

  socket.on('draft:connect', (data) => {
    closeOpen();

    /*haveDraft(data.id).then((have) => {
      if (!have) {
        config.
      }
    })*/
    if (data.name) {
      const response = {
        id: data.id,
        draft: null
      };

      storage
        .haveDraft(data.name)
        .then((haveDraft) => {
          if (haveDraft) {
            return storage.loadDraft(data.name).then((store) => {
              draftStore = store;
              draftId = data.name;
              socket.join(`draft:${data.name}`);

              if (data.getDraft) {
                return draftStore.read();
              } else {
                return false;
              }
            });
          } else {
            return false;
          }
        })
        .then((draft) => {
          if (draft) {
            response.draft = draft;
          }

          socket.emit('draft:current', response);
        });
    }
  });

  socket.on('draft:create', (data) => {
    closeOpen();

    storage.haveDraft(data.name).then(
      (have) => {
        if (have) {
          socket.emit('draft:error', {
            id: data.id,
            message: 'Already have a draft with that id'
          });
        } else {
          storage.loadDraft(data.name).then(
            (store) => {
              draftStore = store;
              draftId = data.name;
              socket.join(`draft:${data.name}`);

              draftStore.save(data);
            },
            (error) => {
              socket.emit('draft:error', {
                id: data.id,
                message: `Error creating draft: ${error.message}`
              });
            }
          );
        }
      },
      (error) => {
        socket.emit('draft:error', {
          id: data.id,
          message: `Error checking for existing drawing: ${error.message}`
        });
      }
    );
  });

  socket.on('draft:draw', (data) => {
    if (!draftId || !draftStore) {
      socket.emit('draft:error', {
        id: data.id,
        type: 'nodraft'
      });
      return;
    }

    if (user) {
      data.user = user;
    }

    // TODO Change to complete?
    if (!data.active) {
      draftStore.update(data).then((storedData) => {
        socket.to(`draft:${draftId}`).emit('draft:draw', data);
      });
    } else {
      socket.to(`draft:${draftId}`).emit('draft:draw', data);
    }
  });

  socket.on('draft:rename', (data) => {});

  socket.on('draft:view', (data) => {
    if (!draftId) {
      return;
    }

    // TODO Create clean view from data

    if (user) {
      data.user = user;
    }

    socket.to(`draft:${draftId}`).emit('draft:view', data);
  });

  return {
    disconnect: () => {}
  };
};
